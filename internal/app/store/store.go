package store


import (
	"database/sql"
	"fmt"
	

	_ "github.com/lib/pq" // anonymously import
)
const (
	host = "localhost"
	port = 5432
	user = "postgres"
	password = "admin"
	dbname = "restapi_dev"
)


//Store...
type Store struct {
	config *Config
	db *sql.DB
}

func New(config *Config) *Store {
	return &Store {
		config: config,
	}
}

func (s *Store) Open() error  {
	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlInfo)
	if err!= nil{
		return err
	}

	if err := db.Ping(); err != nil{
		return err
	}

	s.db = db
	return nil
}
//migrate -path migrations -database "postgres://localhost/restapi_dev?sslmode=disable&port=5432&user=postgres&password=admin" up

func (s *Store) Close()   {
s.db.Close()
}