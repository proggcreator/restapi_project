package model

type order struct {
	OrderUID int 
    Entry string
    InternalSignature string
    Locale string
    CustomerID string
    TrackNumber string
    DeliveryService string
    Shardkey string
    SmID int 

}