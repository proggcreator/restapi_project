CREATE TABLE orders(
    OrderUID integer not null primary key UNIQUE ,
    Entry text,
    InternalSignature text,
    Locale text,
    CustomerID text,
    TrackNumber text,
    DeliveryService text,
    Shardkey text,
    SmID integer
);