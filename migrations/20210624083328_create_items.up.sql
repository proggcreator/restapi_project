CREATE TABLE items(
    ChrtID integer not null  primary key UNIQUE,
    Price integer,
    Rid text,
    Name text,
    Sale int,
    Size text,
    TotalPrice integer,
    NmID integer,
    Brand text

);