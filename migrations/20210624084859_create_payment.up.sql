CREATE TABLE payment(
    Transaction text,
    Currency text,
    Provider text,
    Amount integer,
    PaymentDt integer,
    Bank text,
    DeliveryCost integer,
    GoodsTotal integer
);